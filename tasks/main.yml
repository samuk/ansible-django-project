---
# Role: Django Project
#
# Description:
# Creates an environment for a Django project, installs it and runs
# initialisation commands. Sets up a supervisor job running the project
# using uwsgi.

- name: Install build-essential and python-dev with apt
  apt: name={{ item }} state=present update_cache=yes cache_valid_time=3600
  with_items:
    - build-essential
    - python-dev

- name: Install mysql-client and dev libs with apt
  apt: name={{ item }} state=present update_cache=yes cache_valid_time=3600
  with_items:
    - mysql-client
    - libmysqlclient-dev
  when: not use_sqlite

- name: Install required apt packages
  apt: name={{ item }} state=present update_cache=yes cache_valid_time=3600
  with_items: apt_packages
  when: apt_packages is defined

- name: Create group for project
  group: name={{ name }} state=present

- name: Create namespace directory
  file: >
    dest={{ prefix_dir }}/{{ namespace }}
    mode=755
    owner=root
    group=root
    state=directory

- name: Create user for project
  user: >
    name={{ name }}
    state=present
    shell=/bin/bash
    group={{ name }}
    append=yes
    home={{ prefix_dir }}/{{ namespace }}/{{ name }}

- name: Create project directory
  file: >
    dest={{ prefix_dir }}/{{ namespace }}/{{ name }}
    mode=755
    owner={{ name }}
    group={{ name }}
    state=directory

- name: Create project directory contents
  file: >
    dest={{ prefix_dir }}/{{ namespace }}/{{ name }}/{{ item }}
    mode=755
    owner={{ name }}
    group={{ name }}
    state=directory
    recurse=yes
  with_items:
    - src
    - bin
    - etc
    - data
    - log
    - var/run
    - www/media
    - www/uploads
    - www/static
    - www/templates
    - .virtualenvs

- name: Install setuptools
  shell: wget https://bootstrap.pypa.io/ez_setup.py -O - | sudo python
  when: install_setuptools

- name: Install pip with easy_install
  easy_install: name=pip

- name: Install virtualenv using pip
  pip: name=virtualenv state=present

- name: Create virtualenv
  become_user: "{{ name }}"
  command: virtualenv {{ prefix_dir }}/{{ namespace }}/{{ name }}/.virtualenvs/{{ name }} --python={{ python_version }}
  args:
    creates: "{{ prefix_dir }}/{{ namespace }}/{{ name }}/.virtualenvs/{{ name }}"

- name: Install uwsgi in the virtualenv
  become_user: "{{ name }}"
  pip: name=uwsgi state=present virtualenv={{ prefix_dir }}/{{ namespace }}/{{ name }}/.virtualenvs/{{ name }}

- name: Install project and dependencies with pip
  become_user: "{{ name }}"
  pip: >
    name="{{ pkg }}"
    virtualenv="{{ prefix_dir }}/{{ namespace }}/{{ name }}/.virtualenvs/{{ name }}"
    extra_args="{{ pip_extra_args }}"
    state=latest
  when: not easy_install_project
  notify: Restart uwsgi web

- name: Install project and dependencies with easy_install
  become_user: "{{ name }}"
  command: "{{ prefix_dir }}/{{ namespace }}/{{ name }}/.virtualenvs/{{ name }}/bin/easy_install -ZU {{ easy_install_extra_args }} {{ pkg }}"
  when: easy_install_project
  notify: Restart uwsgi web

- name: Pip install additional python packages
  become_user: "{{ name }}"
  pip: name={{ item }} virtualenv={{ prefix_dir }}/{{ namespace }}/{{ name }}/.virtualenvs/{{ name }}
  with_items: "{{ additional_python_pkgs }}"

- name: Configure settings.py
  template: >
    src=settings.py.j2
    dest={{ prefix_dir }}/{{ namespace }}/{{ name }}/etc/settings.py
    owner={{ name }}
    group={{ name }}
    mode=0644
  notify:
    - Restart uwsgi web
    - Restart celery worker
    - Restart celery inters worker

# We have to create a manage.py, because the Django module for Ansible uses manage.py, not Django-admin.py like we use with Chef
- name: Create a manage.py
  become_user: "{{ name }}"
  template: >
    src=manage.py.j2
    dest={{ prefix_dir }}/{{ namespace }}/{{ name }}/bin/manage.py
    owner={{ name }}
    group={{ name }}
    mode=0755

- name: Create admin login
  become_user: "{{ name }}"
  template: >
    src=admin_user.json.j2
    dest={{ prefix_dir }}/{{ namespace }}/{{ name }}/etc/admin_user.json
    owner={{ name }}
    group={{ name }}
    mode=0644
  when: create_admin_login

- name: Run Sync DB
  become_user: "{{ name }}"
  django_manage: >
    command=syncdb
    app_path={{ prefix_dir }}/{{ namespace }}/{{ name }}/bin
    settings=settings
    pythonpath={{ prefix_dir }}/{{ namespace }}/{{ name }}/etc
    virtualenv={{ prefix_dir }}/{{ namespace }}/{{ name }}/.virtualenvs/{{ name }}
  when: django_admin_syncdb

- name: Run Migrate
  become_user: "{{ name }}"
  django_manage: >
    command=migrate
    app_path={{ prefix_dir }}/{{ namespace }}/{{ name }}/bin
    settings=settings
    pythonpath={{ prefix_dir }}/{{ namespace }}/{{ name }}/etc
    virtualenv={{ prefix_dir }}/{{ namespace }}/{{ name }}/.virtualenvs/{{ name }}
  when: django_admin_migrate

- name: Load fixtures
  become_user: "{{ name }}"
  django_manage: >
    command=loaddata
    app_path={{ prefix_dir }}/{{ namespace }}/{{ name }}/bin
    settings=settings
    pythonpath={{ prefix_dir }}/{{ namespace }}/{{ name }}/etc
    virtualenv={{ prefix_dir }}/{{ namespace }}/{{ name }}/.virtualenvs/{{ name }}
    fixtures={{ prefix_dir }}/{{ namespace }}/{{ name }}/etc/admin_user.json
  when: create_admin_login

- name: Collect static
  become_user: "{{ name }}"
  django_manage: >
    command=collectstatic
    app_path={{ prefix_dir }}/{{ namespace }}/{{ name }}/bin
    settings=settings
    pythonpath={{ prefix_dir }}/{{ namespace }}/{{ name }}/etc
    virtualenv={{ prefix_dir }}/{{ namespace }}/{{ name }}/.virtualenvs/{{ name }}
  when: django_admin_collectstatic

- name: Add django_wsgi template
  template: >
    src=django_wsgi.py.j2
    dest={{ prefix_dir }}/{{ namespace }}/{{ name }}/bin/django_wsgi.py
    owner={{ name }}
    group={{ name }}
    mode=0664

- name: Add django uwsgi run script
  template: >
    src=sv-uwsgi-run.sh.j2
    dest={{ prefix_dir }}/{{ namespace }}/{{ name }}/bin/supervisor-uwsgi-{{ name }}.sh
    owner={{ name }}
    group={{ name }}
    mode=0775

- name: Install supervisor
  pip: name=supervisor

- name: Create supervisor config file
  template: >
    src=supervisor/supervisord.conf
    dest=/etc/supervisord.conf
    owner=root
    group=root
    mode=0664

- name: Create supervisor init script
  copy: >
    src=supervisor_init.txt
    dest=/etc/init.d/supervisor
    owner=root
    group=root
    mode=0775

- name: Create supervisor.d directory
  file: >
    dest=/etc/supervisor.d
    owner=root
    group=root
    mode=0775
    state=directory

- name: Create supervisor log directory
  file: >
    dest=/var/log/supervisor
    owner=root
    group=root
    mode=0775
    state=directory

- name: Create supervisor program for uwsgi
  template: >
    src=supervisor/supervisor_uwsgi.conf
    dest=/etc/supervisor.d/uwsgi_{{ name }}.conf
    owner=root
    group=root
    mode=0664
  notify: Update supervisor

- name: Create extra supervisor programs
  template: >
    src=supervisor/supervisor_program.conf
    dest=/etc/supervisor.d/{{ item.name }}.conf
    owner=root
    group=root
    mode=0664
  notify: Update supervisor
  with_items: extra_supervisor_programs
  when: extra_supervisor_programs is defined

- name: Enable and start supervisor
  service: >
    name=supervisor
    enabled=yes
    state=started
