# Django Project

The Ansible Django Project role will install a Django project, set up uwsgi and create a Supervisor job for it. An nginx/Apache configuration can then be set up to proxy requests to the uwsgi service in order to serve requests.

### Required variables:

##### name

Name of the project

###### Example

    name: windows2000

##### namespace

Name of the client/company

###### Example

    namespace: microsoft

##### pkg

The python package of the Django project

##### settings_import

The import path of the Django settings file

### Optional variables:

##### pip_extra_args

Any extra arguments to pip

###### Example

    pip_extra_args: --index-url https://user:pass@cheeseshop.example.com --pre --allow-external

##### additional_python_pkgs

Any additional python packages to install into the project's virtualenv

###### Example

    additional_python_packages:
      - Pillow
      - pyzmq
      - zmcat


##### additional_python_imports

Additional imports to made in the settings.py file.

##### Example

    additional_python_imports:
      - from urllib import *
      - import boto.ec2.*

##### prefix_dir

Where the package should be installed (default: /opt)

##### create_admin_login

Whether the default admin login should be created (default: yes)

##### django_admin_syncdb

Run syncdb (default: yes)

##### django_admin_migrate

Run south migrations (default: yes)

##### django_admin_collectstatic

Run collectstatic (default: yes)

##### install_setuptools

Install the latest version of setuptools globally (default: yes)

##### uwsgi_listen_int

The interface upon which uwsgi should listen (default: 127.0.0.1)

##### uwsgi_port

The port upon which uwsgi should listen (default: 8080)

##### uwsgi_locale

The LC_ALL and LANG environment variables to set (default: en_US.UTF-8)

##### uwsgi_process_count

The amount of processes uwsgi should run (default: 12)

##### python_version

The full name of the python executable including version (default: python2.7)

##### media_root

MEDIA_ROOT Django settings variable

##### use_sqlite

Use SQLite for the Django database. If 'no', then set the variables below. (default: yes)

##### db_engine

Django DB engine

##### db_name

Django DB name

##### db_user

Django DB user

##### db_password

Django DB password

##### db_host

Django DB host

##### db_port

Django DB port

##### extra_supervisor_programs

A dictionary containing the configuration for any extra supervisor programs to add.

###### Example

    extra_supervisor_programs:
      - name: celery_worker
        command: /usr/local/bin/program.sh
        user: ubuntu
        directory: /usr/local/bin
        redirect_stderr: yes
        stdout_logfile: /var/log/program.log
